package de.pyuu.div.oneshot.util;

public enum GameState {
	
	LOBBY,
	INGAME,
	RESTARTING;

}
