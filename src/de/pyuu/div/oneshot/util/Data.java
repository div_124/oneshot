package de.pyuu.div.oneshot.util;

import java.util.ArrayList;
import java.util.List;

public class Data {

	
	/*
	 * Array-Listen, die Spieler in lebend und tod einteilen
	 */
	
	public static ArrayList<String> alive = new ArrayList<>();
	public static ArrayList<String> death = new ArrayList<>();
	public static ArrayList<String> ingame = new ArrayList<>();
	public static ArrayList<String> inlobby = new ArrayList<>();
	
	
	/*
	 * Lore f�r die verschiedenen Items
	 */
	
	public static List<String> lore = new ArrayList<String>();
	public static List<String> lore_bow = new ArrayList<String>();
	public static List<String> lore_sword = new ArrayList<String>();
	

	
}
