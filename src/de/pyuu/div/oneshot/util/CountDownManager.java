package de.pyuu.div.oneshot.util;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.Listener;
import org.bukkit.scheduler.BukkitTask;

import de.pyuu.div.oneshot.Main;
import de.pyuu.div.oneshot.methods.TitleManager;


public class CountDownManager implements Listener {

	private Main instance;
	private BukkitTask check;
	public static BukkitTask lobbycd;
	public boolean count_started = false;
	public int minPlayer = 1;
	public int count = 60;
	public int startPlayer = 2;
	public boolean count_forced = false;
	public boolean forcestart = false;

	//Diese Klasse wird für die Lobbyphase benötigt.
	/*
	 * Coded by Pyuu 
	 */
	
	
	public CountDownManager(Main instance) {
		this.instance = instance;
	}

	public void checkToStart() {
		this.check = Bukkit.getScheduler().runTaskTimer(this.instance,
				new Runnable() {
					public void run() {
						if (count_started == false) {
							if (Bukkit.getOnlinePlayers().size() >= minPlayer) {

								Player pss = null;
								startCountdown(pss);
							} else {
								if ((minPlayer - Bukkit.getOnlinePlayers()
										.size()) == 1) {
								}
							}
						}
					}
				}, 20L, 40L);
	}

	@SuppressWarnings("static-access")
	public void resetCountdown() {
		this.lobbycd.cancel();
		count_started = false;
		count = 60;

		checkToStart();
	}

	@SuppressWarnings("static-access")
	public void startCountdown(Player ps) {
		this.check.cancel();
		count_started = true;

		try {
			this.lobbycd.cancel();
		} catch (NullPointerException ex) {
		}

		this.lobbycd = Bukkit.getScheduler().runTaskTimer(this.instance,
				new Runnable() {
					public void run() {
						if (Bukkit.getOnlinePlayers().size() < minPlayer) {
							Bukkit.broadcastMessage("§3§l§m------------------------------------------");
							Bukkit.broadcastMessage(Main.PREFIX
									+ " §cEs sind zu wenige Spieler online.");
							Bukkit.broadcastMessage(Main.PREFIX
									+ " §cDer Countdown wird abgebrochen!");
							Bukkit.broadcastMessage("§3§l§m------------------------------------------");

							resetCountdown();
							return;
						}

						if (count_forced == false && forcestart == false) {
							if (Bukkit.getOnlinePlayers().size() >= startPlayer) {
								count_forced = true;
								if (count > 10) {
									count = 10;
									Bukkit.broadcastMessage("§3§l§m------------------------------------------");
									Bukkit.broadcastMessage(Main.PREFIX
											+ " §bEs sind §3"
											+ Bukkit.getOnlinePlayers().size()
											+ " §bSpieler online.");
									Bukkit.broadcastMessage(Main.PREFIX
											+ " §bDer Countdown wurde auf §910 Sekunden§b verkürzt.");
									Bukkit.broadcastMessage("§3§l§m------------------------------------------");
								}
							}
						}

						/*
						 *Countdown Chat-Ausgabe 60,30,10,3-0 
						 */
						
						if (count == 60) {
							for (Player p : Bukkit.getOnlinePlayers()) {
								p.sendMessage(Main.PREFIX
										+ " §bIn §360 Sekunden§b beginnt das Spiel.");
							}
						}

						if (count == 30) {
							for (Player p : Bukkit.getOnlinePlayers()) {
								p.sendMessage(Main.PREFIX
										+ " §bIn §330 Sekunden§b beginnt das Spiel.");
							}
						}

						if (count == 10) {
							for (Player p : Bukkit.getOnlinePlayers()) {
								p.sendMessage(Main.PREFIX
										+ " §bIn §310 Sekunden§b beginnt das Spiel.");
							}
						}

						if (count == 3) {
							for (Player p : Bukkit.getOnlinePlayers()) {
								p.sendMessage(Main.PREFIX
										+ " §bIn §33 Sekunden§b beginnt das Spiel.");
							}
						}

						if (count == 2) {
							for (Player p : Bukkit.getOnlinePlayers()) {
								p.sendMessage(Main.PREFIX
										+ " §bIn §32 Sekunden§b beginnt das Spiel.");
							}
						}

						if (count == 1) {
							for (Player p : Bukkit.getOnlinePlayers()) {
								p.sendMessage(Main.PREFIX
										+ " §bIn §31 Sekunden§b beginnt das Spiel.");
							}
						}

						if (count == 0) {
							instance.startGame();
							CountDownManager.this.lobbycd.cancel();
							
							for(Player ps : Bukkit.getOnlinePlayers()) {
								Data.ingame.add(ps.getName());
							}
							for (Player p : Bukkit.getOnlinePlayers()) {
								TitleManager.sendActionBar(p, "§6§lLos Gehts !");
							}

						}
						
						/*
						 * Countdown Actionbar 30-0
						 */
						
						if (count == 60) {
							for (Player p : Bukkit.getOnlinePlayers()) {
								TitleManager.sendActionBar(p, "§a§lNoch 60 Sekunden bis zum Beginn");
								
							}
						}
						
						if (count == 30) {
							for (Player p : Bukkit.getOnlinePlayers()) {
								TitleManager.sendActionBar(p, "§4> §a§lNoch 30 Sekunden bis zum Beginn §4<");
							}
						}
						
						if (count == 29) {
							for (Player p : Bukkit.getOnlinePlayers()) {
								TitleManager.sendActionBar(p, "§4>    §a§lNoch 29 Sekunden bis zum Beginn    §4<");
							}
						}
						
						if (count == 28) {
							for (Player p : Bukkit.getOnlinePlayers()) {
								TitleManager.sendActionBar(p, "§4> §a§lNoch 28 Sekunden bis zum Beginn §4<");
							}
						}
						
						if (count == 27) {
							for (Player p : Bukkit.getOnlinePlayers()) {
								TitleManager.sendActionBar(p, "§4>    §a§lNoch 27 Sekunden bis zum Beginn    §4<");
							}
						}
						
						if (count == 26) {
							for (Player p : Bukkit.getOnlinePlayers()) {
								TitleManager.sendActionBar(p, "§4> §a§lNoch 26 Sekunden bis zum Beginn §4<");
							}
						}
						
						if (count == 25) {
							for (Player p : Bukkit.getOnlinePlayers()) {
								TitleManager.sendActionBar(p, "§4>    §a§lNoch 25 Sekunden bis zum Beginn    §4<");
							}
						}

						if (count == 24) {
							for (Player p : Bukkit.getOnlinePlayers()) {
								TitleManager.sendActionBar(p, "§4> §a§lNoch 24 Sekunden bis zum Beginn §4<");
							}
						}
						
						if (count == 23) {
							for (Player p : Bukkit.getOnlinePlayers()) {
								TitleManager.sendActionBar(p, "§4>    §a§lNoch 23 Sekunden bis zum Beginn    §4<");
							}
						}
						
						if (count == 22) {
							for (Player p : Bukkit.getOnlinePlayers()) {
								TitleManager.sendActionBar(p, "§4> §a§lNoch 22 Sekunden bis zum Beginn §4<");
							}
						}
						
						if (count == 21) {
							for (Player p : Bukkit.getOnlinePlayers()) {
								TitleManager.sendActionBar(p, "§4>    §a§lNoch 21 Sekunden bis zum Beginn    §4<");
							}
						}
						
						if (count == 20) {
							for (Player p : Bukkit.getOnlinePlayers()) {
								TitleManager.sendActionBar(p, "§4> §a§lNoch 20 Sekunden bis zum Beginn §4<");
							}
						}
						
						if (count == 19) {
							for (Player p : Bukkit.getOnlinePlayers()) {
								TitleManager.sendActionBar(p, "§4>    §a§lNoch 19 Sekunden bis zum Beginn    §4<");
							}
						}
						
						if (count == 18) {
							for (Player p : Bukkit.getOnlinePlayers()) {
								TitleManager.sendActionBar(p, "§4> §a§lNoch 18 Sekunden bis zum Beginn §4<");
							}
						}
						
						if (count == 17) {
							for (Player p : Bukkit.getOnlinePlayers()) {
								TitleManager.sendActionBar(p, "§4>    §a§lNoch 17 Sekunden bis zum Beginn    §4<");
							}
						}
						
						if (count == 16) {
							for (Player p : Bukkit.getOnlinePlayers()) {
								TitleManager.sendActionBar(p, "§4> §a§lNoch 16 Sekunden bis zum Beginn §4<");
							}
						}
						
						if (count == 15) {
							for (Player p : Bukkit.getOnlinePlayers()) {
								TitleManager.sendActionBar(p, "§4>    §a§lNoch 15 Sekunden bis zum Beginn    §4<");
							}
						}
						
						if (count == 14) {
							for (Player p : Bukkit.getOnlinePlayers()) {
								TitleManager.sendActionBar(p, "§4> §a§lNoch 14 Sekunden bis zum Beginn §4<");
							}
						}
						
						if (count == 13) {
							for (Player p : Bukkit.getOnlinePlayers()) {
								TitleManager.sendActionBar(p, "§4>    §a§lNoch 13 Sekunden bis zum Beginn    §4<");
							}
						}

						if (count == 12) {
							for (Player p : Bukkit.getOnlinePlayers()) {
								TitleManager.sendActionBar(p, "§4> §a§lNoch 12 Sekunden bis zum Beginn §4<");
							}
						}
						
						if (count == 11) {
							for (Player p : Bukkit.getOnlinePlayers()) {
								TitleManager.sendActionBar(p, "§4>    §a§lNoch 11 Sekunden bis zum Beginn    §4<");
							}
						}
						
						if (count == 10) {
							for (Player p : Bukkit.getOnlinePlayers()) {
								TitleManager.sendActionBar(p, "§4> §a§lNoch 10 Sekunden bis zum Beginn §4<");
							}
						}
						
						if (count == 9) {
							for (Player p : Bukkit.getOnlinePlayers()) {
								TitleManager.sendActionBar(p, "§4>    §a§lNoch 9 Sekunden bis zum Beginn    §4<");
							}
						}
						
						if (count == 8) {
							for (Player p : Bukkit.getOnlinePlayers()) {
								TitleManager.sendActionBar(p, "§4> §a§lNoch 8 Sekunden bis zum Beginn §4<");
							}
						}
						
						if (count == 7) {
							for (Player p : Bukkit.getOnlinePlayers()) {
								TitleManager.sendActionBar(p, "§4>     §a§lNoch 7 Sekunden bis zum Beginn    §4<");
							}
						}
						
						if (count == 6) {
							for (Player p : Bukkit.getOnlinePlayers()) {
								TitleManager.sendActionBar(p, "§4> §a§lNoch 6 Sekunden bis zum Beginn §4<");
							}
						}
						
						if (count == 5) {
							for (Player p : Bukkit.getOnlinePlayers()) {
								TitleManager.sendActionBar(p, "§4>     §a§lNoch 5 Sekunden bis zum Beginn    §4<");
							}
						}
						
						if (count == 4) {
							for (Player p : Bukkit.getOnlinePlayers()) {
								TitleManager.sendActionBar(p, "§4> §a§lNoch 4 Sekunden bis zum Beginn §4<");
							}
						}

						if (count == 3) {
							for (Player p : Bukkit.getOnlinePlayers()) {
								TitleManager.sendActionBar(p, "§4>     §a§lNoch 3 Sekunden bis zum Beginn    §4<");
							}
						}

						if (count == 2) {
							for (Player p : Bukkit.getOnlinePlayers()) {
								TitleManager.sendActionBar(p, "§4> §a§lNoch 2 Sekunden bis zum Beginn §4<");
							}
						}

						if (count == 1) {
							for (Player p : Bukkit.getOnlinePlayers()) {
								TitleManager.sendActionBar(p, "§4>>>    §a§lNoch 1 Sekunden bis zum Beginn    §4<<<");
							}
						}
						
						/*
						 * Exp-Countdown 60-30
						 */
						
						if(count >= 30) {
							for (Player p : Bukkit.getOnlinePlayers()) {
								p.setLevel(count);
							}
						}
						
						if (count == 30) {
							for (Player p : Bukkit.getOnlinePlayers()) {
								p.setLevel(0);
							}
						}
						
						
						
						count--;
					}
				}, 20L, 20L);
	}

	@SuppressWarnings("unused")
	private float getFloat() {
		return (float) this.count / 0.6F;
	}

}
