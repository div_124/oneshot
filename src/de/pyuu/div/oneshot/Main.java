package de.pyuu.div.oneshot;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.Listener;
import org.bukkit.plugin.java.JavaPlugin;

import de.pyuu.div.oneshot.commands.LobbySpawnCommand;
import de.pyuu.div.oneshot.commands.SetHolostatsCommand;
import de.pyuu.div.oneshot.commands.SetlobbyCommand;
import de.pyuu.div.oneshot.commands.SetSpawnCommand;
import de.pyuu.div.oneshot.commands.SpawnCommand;
import de.pyuu.div.oneshot.listener.BlockedEvents;
import de.pyuu.div.oneshot.listener.DeathListener;
import de.pyuu.div.oneshot.listener.EntityDamageListener;
import de.pyuu.div.oneshot.listener.JoinListener;
import de.pyuu.div.oneshot.listener.QuitListener;
import de.pyuu.div.oneshot.listener.RespawnListener;
import de.pyuu.div.oneshot.methods.PlayItems;
import de.pyuu.div.oneshot.mysql.MySQL;
import de.pyuu.div.oneshot.util.CountDownManager;
import de.pyuu.div.oneshot.util.GameState;



public class Main extends JavaPlugin implements Listener {

	
	public static String PREFIX = "�e�l[�c�lOneShot�e�l] ";
	static Main instance;
	public static  MySQL mysql;
	private CountDownManager cdmanager;
	
	
	
	public static List<String> nicks = new ArrayList<String>();
	
	/*
	 * Permissions:
	 * - os.setlobbyspawn
	 * - os.setspawn
	 * - os.start
	 * - os.setholostats
	 */
	

	@Override
	public void onEnable() {
		
		
		state = GameState.LOBBY;
		
		
		System.out.println("[OneShot] Keine Fehler aufgetreten.");
		System.out.println("[OneShot] Plugin wurde gestartet.");
		instance = this;
		
		// Die Registrierung der Events und Commands:

		this.getServer().getPluginManager().registerEvents(this, this);
		registerCommands();
		registerEvents();
        mysql=new MySQL(getConfig().getString("Config.MySQL.User"),getConfig().getString("Config.MySQL.Password"),getConfig().getString("Config.MySQL.Host"),getConfig().getString("Config.MySQL.DB"));
        mysql.connect();
		mysql.update("CREATE TABLE IF NOT EXISTS Stats(UUID varchar(64), KILLS int, DEATHS int);");
        loadConfig();
		// CountDownManager usw...
		cdmanager = new CountDownManager(this);
		cdmanager.checkToStart();

	}

	@Override
	public void onDisable() {
		System.out.println("[OneShot] Keine Fehler aufgetreten.");
		System.out.println("[OneShot] Plugin wurde gestoppt.");
		instance = null;
	}

	public static Main getInstance() {
		return instance;
	}
    public void loadConfig(){
        getConfig().addDefault("Config.MySQL.Host", "Insert");
        getConfig().addDefault("Config.MySQL.User", "Insert");
        getConfig().addDefault("Config.MySQL.Password", "Insert");
        getConfig().addDefault("Config.MySQL.DB", "Insert");
        getConfig().options().copyDefaults(true);
        saveConfig();
      }
	
	
	public void registerEvents() {
		this.getServer().getPluginManager().registerEvents(new EntityDamageListener(), this);
		this.getServer().getPluginManager().registerEvents(new BlockedEvents(), this);
		this.getServer().getPluginManager().registerEvents(new DeathListener(), this);
		this.getServer().getPluginManager().registerEvents(new QuitListener(), this);
		this.getServer().getPluginManager().registerEvents(new JoinListener(), this);
		this.getServer().getPluginManager().registerEvents(new RespawnListener(), this);
	}

	public void registerCommands() {
		this.getCommand("setspawn").setExecutor(new SetSpawnCommand());
		this.getCommand("setlobbyspawn").setExecutor(new SetlobbyCommand());
		this.getCommand("setholostats").setExecutor(new SetHolostatsCommand());
		this.getCommand("spawn").setExecutor(new SpawnCommand());
		this.getCommand("lobbyspawn").setExecutor(new LobbySpawnCommand());
	}
	

	public static void startGame() {
		
		state=GameState.INGAME;
		
		for (Player ps : Bukkit.getOnlinePlayers()) {
			ps.getInventory().clear();
			ps.sendMessage(PREFIX + "�a Das Spiel wurde gestartet!");
			ps.performCommand("spawn");		
			
			PlayItems.setPlayItems(ps);

		}
	}
	public static GameState state;

}
