package de.pyuu.div.oneshot.listener;

import org.bukkit.GameMode;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerQuitEvent;

import de.pyuu.div.oneshot.Main;
import de.pyuu.div.oneshot.util.Data;

public class QuitListener implements Listener {
	
	@EventHandler
	public void onQuit(PlayerQuitEvent e) {
		Player p = e.getPlayer();
		
		if(Data.inlobby.contains(p.getName())) {
			Data.inlobby.remove(p.getName());
		}
		if(Data.ingame.contains(p.getName())) {
			Data.ingame.remove(p.getName());
		}
		if(Data.death.contains(p.getName())) {
			Data.death.remove(p.getName());
		}
		if(Data.alive.contains(p.getName())) {
			Data.alive.add(p.getName());
		}
		e.setQuitMessage(Main.PREFIX + p.getDisplayName() + "�c�l hat das Spiel verlassen.");
		
		p.setGameMode(GameMode.SURVIVAL);
	}

}
