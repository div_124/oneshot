package de.pyuu.div.oneshot.listener;

import org.bukkit.GameMode;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import de.pyuu.div.oneshot.Main;
import de.pyuu.div.oneshot.mysql.HoloStats;
import de.pyuu.div.oneshot.mysql.SQLStats;
import de.pyuu.div.oneshot.util.Data;

public class JoinListener implements Listener {
	
	
	
	@EventHandler
	public void onJoin(PlayerJoinEvent e) {
		Player p = e.getPlayer();
		p.getInventory().clear();
		
		p.setHealth(20D);
		p.setFoodLevel(20);
		
		/*
		 * Definition, des Back-to-hub items
		 */
		
		SQLStats.createPlayer(p.getUniqueId().toString());
		
		ItemStack cream = new ItemStack(Material.MAGMA_CREAM);
		ItemMeta creammeta = cream.getItemMeta();
		creammeta.setDisplayName("�c�lZur�ck zur Lobby");
		Data.lore.add("�7�lRechtsklicken um dieses Item zu benutzen!");
		creammeta.setLore(Data.lore);
		cream.setItemMeta(creammeta);
		new HoloStats(p);
		
		/*
		 * Gamemode-�berpr�fung des Spielers und ggf. versetzung in den Suvival-Mode
		 */
		
		if(!Data.inlobby.contains(p.getName())) {
			Data.inlobby.add(p.getName());
		}
		if(Data.ingame.contains(p.getName())) {
			Data.ingame.remove(p.getName());
		}
		if(Data.death.contains(p.getName())) {
			Data.death.remove(p.getName());
		}
		if(!Data.alive.contains(p.getName())) {
			Data.alive.add(p.getName());
			p.setGameMode(GameMode.SURVIVAL);
		}
		e.setJoinMessage(Main.PREFIX + p.getName() + " �a�lhat das Spiel betreten.");
		p.getInventory().setItem(8, cream);
		p.performCommand("lobbyspawn");
	}

}
