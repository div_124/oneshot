package de.pyuu.div.oneshot.listener;

import org.bukkit.GameMode;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerRespawnEvent;

import de.pyuu.div.oneshot.methods.DeathItems;

public class RespawnListener implements Listener {
	
	
	/*
	 * spieler wird beim Respen-Event in den Spectator-Modus gesetzt
	 */
	
	@EventHandler
	public void onRespawn(PlayerRespawnEvent e) {
	Player p = e.getPlayer();
	p.setGameMode(GameMode.SPECTATOR);
	DeathItems.setDeathItems(p);
	
	return;
	
	}
}
