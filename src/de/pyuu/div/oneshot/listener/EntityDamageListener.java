package de.pyuu.div.oneshot.listener;

import org.bukkit.entity.Arrow;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.ProjectileHitEvent;

import de.pyuu.div.oneshot.Main;
import de.pyuu.div.oneshot.util.Data;
import de.pyuu.div.oneshot.util.GameState;

public class EntityDamageListener implements Listener {
	
	@EventHandler
	public void onArrowShoot(EntityDamageByEntityEvent e) {
		if (((e.getEntity() instanceof Player))
				&& ((e.getDamager() instanceof Arrow))) {
			Arrow arrow = (Arrow) e.getDamager();
			if ((arrow.getShooter() instanceof Player)) {
				e.setDamage(500.0D);
			}

		}

		Player p = (Player) e.getEntity();
		if (Data.death.contains(p.getName())) {
			e.setCancelled(true);
		}

		if (Main.state == GameState.LOBBY) {
			e.setCancelled(true);
		} else {
			e.setCancelled(false);
		}
	}

	@EventHandler
	public void onProjectileHit(ProjectileHitEvent e) {

		if ((e.getEntity() instanceof Arrow)) {
			Arrow arrow = (Arrow) e.getEntity();
			if ((arrow.getShooter() instanceof Player)) {
				arrow.remove();
			}
		}
	}
}
