package de.pyuu.div.oneshot.listener;


import java.util.Random;

import org.bukkit.Bukkit;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.PlayerDeathEvent;



import de.pyuu.div.oneshot.Main;
import de.pyuu.div.oneshot.methods.PlayItems;
import de.pyuu.div.oneshot.mysql.SQLStats;
import de.pyuu.div.oneshot.util.Data;
import de.pyuu.div.oneshot.util.GameState;

public class DeathListener implements Listener {
	
	Integer counter;
	int timer = 10;

	
	@EventHandler
	public void Event_Death(PlayerDeathEvent e) {
		Player p = e.getEntity();
		Player killer = p.getKiller();
		
		/*
		 * Eintragung der Stats in die Holo-Statstafel
		 */
		
		if (p.getKiller() instanceof Player) {
			SQLStats.addDeaths(p.getUniqueId().toString(), 1);
			SQLStats.addKills(p.getKiller().getUniqueId().toString(), 1);
		} else {
			SQLStats.addDeaths(p.getUniqueId().toString(), 1);

		}
		/*
		 * Death-Nachricht wird �berschrieben, Itemdrop wird gesperrt
		 */
		e.setDeathMessage(null);
		e.getDrops().clear();
		
		/*
		 * Eintragung in die jeweiligen status-Listen
		 */
		
		if(p.isDead()) {
			if(Data.alive.contains(p.getName())) {
				Data.alive.remove(p.getName());
			}
			if(!Data.death.contains(p.getName())) {
				Data.death.add(p.getName());
			}
			
			PlayItems.setPlayItems(killer);
			
			
			killer.playSound(killer.getLocation(), Sound.LEVEL_UP, 1.0F, 1.0F);
			
			/*
			 * Zufallig ausgesuchte Todesmeldungen
			 */
			
			Random r = new Random();
			int zufall = r.nextInt(4);
			switch(zufall) {
		
			case 0:
				Bukkit.getServer().broadcastMessage(Main.PREFIX + "�c" + p.getName() + " �6konnte dem Tod nicht wiederstehen.");
				break;
			case 1:
				Bukkit.getServer().broadcastMessage(Main.PREFIX + "�a" + killer.getName() + " �6hat �c" + p.getName() + " �6erldigt");
				break;
			case 2:
				Bukkit.getServer().broadcastMessage(Main.PREFIX + "�8�c" + p.getName() + " �6schaut sich die Radischen jetzt von unten an !");
				break;
			case 3:
				Bukkit.getServer().broadcastMessage(Main.PREFIX + "�a" + killer.getName() + " �ahat �c" + p.getName() + " �aentsaftet !");
				break;
			}
			
			/*
			 * Bestimmung des Gewinnder und neustarten des Server um erneuten Spielbeginn einzuleiten
			 */
			
			if(Data.death.size() == Bukkit.getOnlinePlayers().size() -1) {
				Bukkit.broadcastMessage("�a�lDer Spieler �c�l" + p.getKiller().getName()
						+ " �a�lhat Gewonnen!");
				
				Main.state=GameState.RESTARTING;
			}
			counter = Bukkit.getScheduler().scheduleSyncRepeatingTask(Main.getInstance(), new Runnable() {
				
				@Override
				public void run() {
					timer--;
					if(timer != 0) {
						for(Player all : Bukkit.getOnlinePlayers()) {
							all.sendMessage("�c�lDer Server startet in �e�l" + timer + " �c�lneu!");
						}
					}else if(timer == 0) {
						Bukkit.shutdown();
					}
				}
			}, 20L, 60L);
		}
		
	}
	

}
