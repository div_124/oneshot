package de.pyuu.div.oneshot.methods;

import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;


public class PlayItems {
	
	public static void setPlayItems(Player p ) {
		
		/*
		 * Erstellung der Haupt-Spielitems
		 */
		
		ItemStack sword = new ItemStack(Material.WOOD_SWORD);
		ItemMeta swordmeta = sword.getItemMeta();
		swordmeta.setDisplayName("�8[�bOneShot�8] �aNahkampfwaffe");
		sword.setItemMeta(swordmeta);
		
		ItemStack bow = new ItemStack(Material.BOW);
		ItemMeta bowmeta = bow.getItemMeta();
		bowmeta.setDisplayName("�8[�bOneShot�8] �aFernkampfwaffe");
		bow.setItemMeta(bowmeta);
		
		ItemStack arrow = new ItemStack(Material.ARROW);
		ItemMeta arrowmeta = arrow.getItemMeta();
		arrowmeta.setDisplayName("�8[�bOneShot�8] �aPfeil f�r deinen Bogen !");
		arrow.setItemMeta(arrowmeta);
		
		
		p.getInventory().setItem(0, bow);
		p.getInventory().setItem(8, sword);
		p.getInventory().setItem(4, arrow);
		
	}

}
