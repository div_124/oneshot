package de.pyuu.div.oneshot.methods;

import org.bukkit.Bukkit;

import de.pyuu.div.oneshot.Main;
import de.pyuu.div.oneshot.util.Data;

public class Announce {
	
	/*
	 * Kontinuirliche informaten �ber lebende Spieler
	 */
	
	public static void DisplayAnnounce() {
		final int alive = Data.alive.size();
		
		Bukkit.getScheduler().scheduleSyncRepeatingTask(Main.getInstance(), new Runnable() {
			
			@Override
			public void run() {
				
				
				
				Bukkit.broadcastMessage(Main.PREFIX + "�a�l Es leben noch �c" + alive + "�8/�c12");
				
			}
		}, 1200L, 1200L);
		
	}

}
