package de.pyuu.div.oneshot.commands;

import java.io.File;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.event.Listener;

import de.pyuu.div.oneshot.Main;

public class SpawnCommand implements Listener, CommandExecutor {

	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {

		if (!(sender instanceof Player)) {
			System.out
					.println("Du kannst einen SpawnPunk nur als Spieler setzen.");

			return true;
		}
		Player p = (Player) sender;

		/*
		 * Config-path (Spawn)
		 */

		File file = new File("plugins//OneShot//Spawn.yml");
		if (!file.exists()) {
			p.sendMessage(Main.PREFIX + "�c�l Es wurde kein Spawn gesetzt.");
			return true;
		}
		YamlConfiguration cfg = YamlConfiguration.loadConfiguration(file);
		Location loc = p.getLocation();

		/*
		 * Koordinaten werden zum Teleportieren aus der Config gelesen
		 */
		
		double x = cfg.getDouble("X");
		double y = cfg.getDouble("Y");
		double z = cfg.getDouble("Z");
		double yaw = cfg.getDouble("Yaw");
		double pitch = cfg.getDouble("Pitch");
		String worldname = cfg.getString("Worldname");

		World world = Bukkit.getWorld(worldname);

		/*
		 * Der Spieler wird in das Spiel Teleportiert
		 */
		
		loc.setX(x);
		loc.setY(y);
		loc.setZ(z);
		loc.setYaw((float) yaw);
		loc.setPitch((float) pitch);
		loc.setWorld(world);

		p.teleport(loc);
		p.setHealth(20);
		return true;
	}
}
