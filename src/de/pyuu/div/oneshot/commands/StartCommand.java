package de.pyuu.div.oneshot.commands;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.Listener;

import de.pyuu.div.oneshot.Main;
import de.pyuu.div.oneshot.util.CountDownManager;

public class StartCommand implements Listener, CommandExecutor {

	public boolean onCommand(CommandSender sender, Command cmd, String label,
			String[] args) {

		if (!(sender instanceof Player)) {
			System.out
					.println("Du kannst einen SpawnPunk nur als Spieler setzen.");

			return true;
		}
		Player p = (Player) sender;
			if (!p.hasPermission("os.start")) {
				p.sendMessage(Main.PREFIX
						+ "�c�l Du hast keine Berechtigung diesen Befehl auszuf�hren.");
				return true;
			}
			
		/*
		 * Countdown wird gestoptt, Spiel wird sofort gestartet
		 */
			
			CountDownManager.lobbycd.cancel();

			Main.startGame();
		return true;
	}
}