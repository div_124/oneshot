package de.pyuu.div.oneshot.mysql;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;

import de.pyuu.div.oneshot.Main;
import de.pyuu.div.oneshot.api.Hologram;


public class HoloStats implements Runnable {

	Player p;

	public HoloStats(Player p) {
		this.p = p;
		Bukkit.getScheduler().runTaskLater(Main.getInstance(), this, 20 * 2);
	}

	@Override
	public void run() {

		File file = new File("plugins//OneShot//HoloStats.yml");
		if (!file.exists()) {
			p.sendMessage(Main.PREFIX
					+ "�c�l Es wurde kein Holo - Punkt gesetzt.");
			return;
		}
		YamlConfiguration cfg = YamlConfiguration.loadConfiguration(file);
		Location loc = p.getLocation();

		double x = cfg.getDouble("X");
		double y = cfg.getDouble("Y");
		double z = cfg.getDouble("Z");
		double yaw = cfg.getDouble("Yaw");
		double pitch = cfg.getDouble("Pitch");
		String worldname = cfg.getString("Worldname");

		World world = Bukkit.getWorld(worldname);

		loc.setX(x);
		loc.setY(y);
		loc.setZ(z);
		loc.setYaw((float) yaw);
		loc.setPitch((float) pitch);
		loc.setWorld(world);

		int deaths = SQLStats.getDeaths(p.getUniqueId().toString());
		int kills = SQLStats.getKills(p.getUniqueId().toString());

		double KD = ((double) kills) / ((double) deaths);
		String kd = Double.valueOf(KD).toString();

		List<String> lines = new ArrayList<>();
		lines.add("�6�lDeine OneShot Stats:");
		lines.add("�7�l]]================[[");
		lines.add("�a�lKills: �e�l" + kills);
		lines.add("�c�lDeaths: �e�l" + deaths);
		lines.add("�4�lKD: �e�l" + kd);
		lines.add("�7�l]]================[[");
		lines.add("�c�lXevento - Netzwerk ");

		Hologram holo = new Hologram(loc, lines);
		holo.display(p);
		return;
	}
}
